"use strict";

let name = prompt("Введіть ім'я:");
let age = prompt("Введіть вік:");

while (name === "" || isNaN(age)) {
  name = prompt("Будь ласка, введіть своє ім'я");
  age = parseInt(prompt("Будь ласка, введіть свій вік"));
}

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age <= 22) {
  const sure = confirm("Are you sure you want to continue?");
  sure === true
    ? alert("Welcome " + name)
    : alert("You are not allowed to visit this website");
} else {
  alert("Welcome " + name);
}
